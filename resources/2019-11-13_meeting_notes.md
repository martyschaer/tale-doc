# 2019-11-13 Meeting Notes
## Remarks
* 4.1: Already too low-level. Remember for future projects to define higher level processes
* 5.4: Don't list technical "constraints", only constraints set by the project environment

## To change
* 5.1.1: Only note sources for requirements, this has a place in the final document
* 5.5.8: Replace business model with business object model in text

## For next meeting
Prepare a draft table of contents for the final document.
