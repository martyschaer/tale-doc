# tale-doc
This repository contains the project documentation for [tale, an experimental version control system](https://gitlab.com/severinkaderli/tale)

## Documents 
* [Requirements Specification](https://gitlab.com/martyschaer/tale-doc/builds/artifacts/master/raw/requirements-specification.pdf?job=PDF)
* [Final Report](https://gitlab.com/martyschaer/tale-doc/builds/artifacts/master/raw/final-report.pdf?job=PDF)
