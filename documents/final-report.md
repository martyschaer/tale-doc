---
title: "tale"
subtitle: "Final Report"
author:
    - Marius Schär
    - Severin Kaderli
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lecturer: "Joachim Wolfgang Kaltz"
lang: "en-UK"
toc: true
lot: true
rule-color: CDDC39
link-color: 448AFF
glossary: true
glossary-file: glossary.tex
...

# Initial Situation
- from the project plan

# Analysis
- summary of reqspec

# Implementation

## Function Requirements
- functional requirements planned vs. actual

## Delivery Objects
- generated artifacts binary, documentation, etc.

## Domain Object Model
- DOM, changes made during development 

## Patch Generation
- diffing algorithm (sourcing, fixes, explained)
- formatting

## Testing
- CI/CD
- testing goals
- testing methodology

## GraalVM Native Images
- How it works
- Why we use it


# Projektmanagementumsetzung
- Schedule and Tasks
- Explain methodology

## Planned

## Actual

## Differences

# Lessons Learned
- summary of lessons leared
  - in implementation
  - in project managment
  - with reference
